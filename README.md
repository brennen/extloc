# extloc - find out where MediaWiki extensions are deployed in Wikimedia production

Running tool: https://extloc.toolforge.org/

Admin interface, for those with access: https://toolsadmin.wikimedia.org/tools/id/extloc

Original task: [T296050](https://phabricator.wikimedia.org/T296050)

## Code

This is all absolutely garbage Python.  Or at least it is probably
un-idiomatic, redundant, and very clunky.

  - `app.py` is a simple Flask app that displays:
    - A list of wikis
    - A list of extensions
    - Wikis for an extension
    - Extensions for a wiki
    - Extensions for a group
    - The underlying JSON data

  - `scrape.py` gets the underlying JSON data by querying the API on each
    wiki on a daily timer.

## Background

The goal here was a thing to expose:

  - What wikis an extension is deployed to
  - What groups an extension is deployed within
  - What extensions are deployed to a wiki
  - What extensions are deployed to a group

This exists, and has a list of extensions: https://www.mediawiki.org/wiki/API:Siteinfo

You can see this list with something like:

```sh
curl 'https://www.mediawiki.org/w/api.php?action=query&meta=siteinfo&siprop=extensions&sifilteriw=local&format=json' -o siteinfo.json
jq '.query.extensions[].name' ./siteinfo.json
```

I didn't want to scrape each individual wiki, but deriving this from
`CommonSettings.php` and `InitialiseSettings.php` [would be tricky][config].

Once I resigned myself to scraping, bd808 [laid out a reasonable plan][bd808]:

  - Schedule a job to:
    - Loop over wikis from dblists or equivalent
    - Scrape API for extension list
    - Stash data
  - Present data to user

[config]: https://www.mediawiki.org/wiki/Manual:Configuration_settings
[bd808]: https://federation.p1k3.com/@bd808@octodon.social/110154276194169100

## Toolforge Build Service

As of 2024-08-06, this uses the [Toolforge Build Service][build-service],
albeit probably not optimally.  In particular it's still mounting
`/data/project/extloc` and stashing data in JSON files there, as well as
getting database credentials out of `my.replica.cnf` in that directory.

[build-service]: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service

### Building a new version of the image

Push changes to `main` on GitLab, and then:

```sh
toolforge build start https://gitlab.wikimedia.org/brennen/extloc
```

### Starting the web service

```sh
toolforge webservice buildservice start --mount all
```

### Scheduling the scraping job

```sh
toolforge jobs run --mount=all --filelog --image tool-extloc/tool-extloc:latest --command "scrape" --schedule "@daily" extloc-scrape
```

## Relevant docs

### Toolforge

  - https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service
  - https://wikitech.wikimedia.org/wiki/Help:Toolforge/Build_Service/My_first_Buildpack_Python_tool
  - Scheduling a job: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Jobs_framework
  - Metadata database: https://wikitech.wikimedia.org/wiki/Help:Toolforge/Database#Metadata_database

### Dependencies

  - https://wikitech.wikimedia.org/wiki/User:Legoktm/toolforge_library
  - https://pymysql.readthedocs.io/
  - https://python-toolforge.readthedocs.io/

## Installation

https://toolsadmin.wikimedia.org/tools/id/extloc

## License

Licensed under the GPL-3.0-or-later license. See COPYING for the full license.
