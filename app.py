import os
import json
from flask import Flask, request, Response, render_template

data_path = os.environ["TOOL_DATA_DIR"] + '/'

app = Flask(__name__)


@app.route('/')
def home():
    wiki_data = json.load(open(data_path + 'wiki-to-extensions.json'))
    ext_data = json.load(open(data_path + 'extension-to-wikis.json'))
    group_data = json.load(open(data_path + 'group-to-extensions.json'))
    return render_template(
        'index.html',
        wikis=wiki_data.keys(),
        extensions=ext_data,
        groups=group_data
        )


@app.route('/wikis')
def wikis():
    """An HTML list of wikis."""
    wiki_data = json.load(open(data_path + 'wiki-to-extensions.json'))
    return render_template('wikis.html', wikis=wiki_data.keys())


@app.route('/wikis.json')
def wikis_json():
    with open(data_path + 'wiki-to-extensions.json') as f:
        data = f.read()
    return Response(data, mimetype='application/json')


@app.route('/extensions')
def extensions():
    """An HTML list of extensions."""
    ext_data = json.load(open(data_path + 'extension-to-wikis.json'))
    extension_names = sorted(ext_data.keys())
    return render_template('extensions.html', extensions=extension_names)


@app.route('/extensions.json')
def extensions_json():
    """JSON extension data."""
    with open(data_path + 'extension-to-wikis.json') as f:
        data = f.read()
    return Response(data, mimetype='application/json')


@app.route('/groups/<string:group_name>')
def show_group(group_name):
    """
    List the extensions for a particular group.
    """
    group_to_extensions = json.load(
            open(data_path + 'group-to-extensions.json')
            )

    if group_name in group_to_extensions.keys():
        # Return the list of extensions for the group, defaulting to an empty
        # list if we don't have any data:
        ext_list = group_to_extensions.get(group_name, [])
        return render_template(
            'group.html',
            group=group_name,
            extensions=ext_list
            )
    app.abort(404, "group not found")


@app.route('/wikis/<string:wiki_name>')
def show_wiki(wiki_name):
    """
    List the extensions for a particular wiki.
    """
    wikis_to_extensions = json.load(
        open(data_path + 'wiki-to-extensions.json')
        )

    if wiki_name in wikis_to_extensions.keys():
        # Return the list of extensions for the wiki, defaulting to an empty
        # list if we don't have any data:
        ext_list = wikis_to_extensions.get(wiki_name, [])
        return render_template(
            'wiki.html',
            wiki=wiki_name,
            extensions=ext_list
            )
    app.abort(404, "wiki not found")


@app.route('/wikis/<string:wiki_name>.json')
def show_wiki_json(wiki_name):
    """
    List the extensions for a particular wiki.
    """
    wikis_to_extensions = json.load(
        open(data_path + 'wiki-to-extensions.json')
        )

    # Return the list of extensions for the wiki, defaulting to an empty list
    # if we don't have any data:
    ext_list = wikis_to_extensions.get(wiki_name, [])
    return Response(json.dumps(ext_list), mimetype='application/json')


@app.route('/extensions/<string:ext_name>')
def show_extension(ext_name):
    """
    List the wikis for a particular extension.
    """
    with open(data_path + '/extension-to-wikis.json') as f:
        extensions_to_wikis = json.load(f)

    with open(data_path + '/extension-to-groups.json') as f:
        extension_to_groups = json.load(f)

    groups = extension_to_groups.get(ext_name, [])

    if ext_name in extensions_to_wikis.keys():
        # Return the list of wikis for the extension, defaulting to an empty
        # list if we don't have any data:
        wikis = extensions_to_wikis.get(ext_name, [])
        return render_template(
            'extension.html',
            extension=ext_name,
            wikis=wikis,
            groups=groups
            )
    app.abort(404, "extension not found")


@app.route('/extensions/<string:ext_name>.json')
def show_extension_json(ext_name):
    """
    List the wikis for a particular extension.
    """
    with open(data_path + 'extension-to-wikis.json') as f:
        extensions_to_wikis = json.load(f)

    # Return the list of wikis for the extension, defaulting to an empty list
    # if we don't have any data:
    ext_list = extensions_to_wikis.get(ext_name, [])
    return Response(json.dumps(ext_list), mimetype='application/json')


if __name__ == '__main__':
    app.run(debug=True)
