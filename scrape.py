#!/usr/bin/env python3

import json
import os
import pymysql
import re
import requests
import toolforge

toolforge.set_user_agent('extloc', 'https://extloc.toolforge.org')

data_path = os.environ["TOOL_DATA_DIR"] + '/'

api_path = '/w/api.php?action=query&meta=siteinfo&siprop=extensions&sifilteriw=local&format=json'

base_url_list = {
    "beta-aawiki": "https://aa.wikipedia.beta.wmflabs.org/",
    "beta-apiportalwiki": "https://api.wikimedia.beta.wmflabs.org/",
    "beta-arwiki": "https://ar.wikipedia.beta.wmflabs.org/",
    "beta-bnwiki": "https://bn.wikipedia.beta.wmflabs.org/",
    "beta-cawiki": "https://ca.wikipedia.beta.wmflabs.org/",
    "beta-commonswiki": "https://commons.wikimedia.beta.wmflabs.org/",
    "beta-crhwiki": "https://crh.wikipedia.beta.wmflabs.org/",
    "beta-cswiki": "https://cs.wikipedia.beta.wmflabs.org/",
    "beta-dewiki": "https://de.wikipedia.beta.wmflabs.org/",
    "beta-dewiktionary": "https://de.wiktionary.beta.wmflabs.org/",
    "beta-en_rtlwiki": "https://en-rtl.wikipedia.beta.wmflabs.org/",
    "beta-enwiki": "https://en.wikipedia.beta.wmflabs.org/",
    "beta-enwikibooks": "https://en.wikibooks.beta.wmflabs.org/",
    "beta-enwikinews": "https://en.wikinews.beta.wmflabs.org/",
    "beta-enwikiquote": "https://en.wikiquote.beta.wmflabs.org/",
    "beta-enwikisource": "https://en.wikisource.beta.wmflabs.org/",
    "beta-enwikiversity": "https://en.wikiversity.beta.wmflabs.org/",
    "beta-enwikivoyage": "https://en.wikivoyage.beta.wmflabs.org/",
    "beta-enwiktionary": "https://en.wiktionary.beta.wmflabs.org/",
    "beta-eowiki": "https://eo.wikipedia.beta.wmflabs.org/",
    "beta-eswiki": "https://es.wikipedia.beta.wmflabs.org/",
    "beta-eswikibooks": "https://es.wikibooks.beta.wmflabs.org/",
    "beta-fawiki": "https://fa.wikipedia.beta.wmflabs.org/",
    "beta-foundationwiki": "https://foundation.wikimedia.beta.wmflabs.org/",
    "beta-hewiki": "https://he.wikipedia.beta.wmflabs.org/",
    "beta-hewiktionary": "https://he.wiktionary.beta.wmflabs.org/",
    "beta-hiwiki": "https://hi.wikipedia.beta.wmflabs.org/",
    "beta-incubatorwiki": "https://incubator.wikimedia.beta.wmflabs.org/",
    "beta-jawiki": "https://ja.wikipedia.beta.wmflabs.org/",
    "beta-kowiki": "https://ko.wikipedia.beta.wmflabs.org/",
    "beta-loginwiki": "https://login.wikimedia.beta.wmflabs.org/",
    "beta-metawiki": "https://meta.wikimedia.beta.wmflabs.org/",
    "beta-nlwiki": "https://nl.wikipedia.beta.wmflabs.org/",
    "beta-ruwiki": "https://ru.wikipedia.beta.wmflabs.org/",
    "beta-simplewiki": "https://simple.wikipedia.beta.wmflabs.org/",
    "beta-sqwiki": "https://sq.wikipedia.beta.wmflabs.org/",
    "beta-srwiki": "https://sr.wikipedia.beta.wmflabs.org/",
    "beta-svwiki": "https://sv.wikipedia.beta.wmflabs.org/",
    "beta-testwiki": "https://test.wikipedia.beta.wmflabs.org/",
    "beta-ukwiki": "https://uk.wikipedia.beta.wmflabs.org/",
    "beta-viwiki": "https://vi.wikipedia.beta.wmflabs.org/",
    "beta-votewiki": "https://vote.wikipedia.beta.wmflabs.org/",
    "beta-wikidatawiki": "https://wikidata.wikipedia.beta.wmflabs.org/",
    "beta-wikifunctionswiki": "https://wikifunctions.wikipedia.beta.wmflabs.org/",
    "beta-zhwiki": "https://zh.wikipedia.beta.wmflabs.org/",
    "beta-zhwikivoyage": "https://zh.wikivoyage.beta.wmflabs.org/",
}


# https://noc.wikimedia.org/conf/dblists/group0.dblist
# https://noc.wikimedia.org/conf/dblists/group1.dblist
# https://noc.wikimedia.org/conf/dblists/group2.dblist
def get_group(name):
    """Get the wikis for a give group name from noc dblists."""
    base_url = 'https://noc.wikimedia.org/conf/dblists/'
    r = requests.get(f"{base_url}{name}.dblist")

    wikis = []

    # dblists are one wiki per line, but may have comments with a leading `#`
    for line in r.text.split("\n"):
        if re.match("^[a-z_]+$", line):
            wikis.append(line)

    return wikis


def get_urls():
    conn = toolforge.connect(
            'meta',
            'analytics',
            cursorclass=pymysql.cursors.DictCursor,
            read_default_file=data_path + 'replica.my.cnf'
            )

    urls = base_url_list

    # conn is a pymysql.connection object.
    with conn.cursor() as cur:
        cur.execute('SELECT * FROM wiki')
        for row in cur:
            urls[row['dbname']] = row['url']

    conn.close()

    return urls


def get_extensions(wiki_base_url):
    r = requests.get(f"{wiki_base_url}{api_path}")
    return r.json()['query']['extensions']


def write_json(filename, data):
    with open(data_path + filename, 'w') as f:
        json.dump(data, f, indent=4)


def main():
    wiki_to_extensions = {}
    extension_to_wikis = {}
    group_to_wikis = {}
    wiki_to_group = {}
    extension_to_groups = {}
    group_to_extensions = {}

    # Maybe this should just offer all dblists - it seems potentially useful
    for group in ['testwikis', 'group0', 'group1', 'group2']:
        group_to_wikis[group] = get_group(group)

    for group in group_to_wikis:
        for wiki in group_to_wikis[group]:
            wiki_to_group[wiki] = group

    urls = get_urls()

    for wiki in urls:
        print(f"checking {wiki}")
        wiki_to_extensions[wiki] = []
        for ext in get_extensions(urls[wiki]):
            wiki_to_extensions[wiki].append(ext['name'])
            extension_to_wikis.setdefault(ext['name'], []).append(wiki)
            if wiki in wiki_to_group:
                group = wiki_to_group[wiki]

                if ext['name'] not in group_to_extensions.get(group, []):
                    group_to_extensions.setdefault(group, []).append(ext['name'])

                if group not in extension_to_groups.get(ext['name'], []):
                    extension_to_groups.setdefault(ext['name'], []).append(group)

    write_json('group-to-extensions.json', group_to_extensions)
    write_json('extension-to-groups.json', extension_to_groups)
    write_json('wiki-to-extensions.json', wiki_to_extensions)
    write_json('extension-to-wikis.json', extension_to_wikis)


main()
